.TH SQ 1 0.34.0 "Sequoia PGP" "User Commands"
.SH NAME
sq key generate \- Generate a new key
.SH SYNOPSIS
.br
\fBsq key generate\fR [\fIOPTIONS\fR]  
.SH DESCRIPTION
Generate a new key.
.PP
Generating a key is the prerequisite to receiving encrypted messages
and creating signatures.  There are a few parameters to this process,
but we provide reasonable defaults for most users.
.PP
When generating a key, we also generate a revocation certificate.
This can be used in case the key is superseded, lost, or compromised.
It is a good idea to keep a copy of this in a safe place.
.PP
After generating a key, use `sq toolbox extract\-cert` to get the
certificate corresponding to the key.  The key must be kept secure,
while the certificate should be handed out to correspondents, e.g. by
uploading it to a key server.
.PP
By default a key expires after 3 years.
Using the `\-\-expiry=` argument specific validity periods may be defined.
It allows for providing a point in time for validity to end or a validity
duration.
.PP
`sq key generate` respects the reference time set by the top\-level
`\-\-time` argument.  It sets the creation time of the key, any
subkeys, and the binding signatures to the reference time.
.PP


.SH OPTIONS
.SS "Subcommand options"
.TP
\fB\-c\fR, \fB\-\-cipher\-suite\fR=\fICIPHER\-SUITE\fR
Select the cryptographic algorithms for the key
.TP
\fB\-\-can\-authenticate\fR
Add an authentication\-capable subkey (default)
.TP
\fB\-\-can\-encrypt\fR=\fIPURPOSE\fR
Add an encryption\-capable subkey. Encryption\-capable subkeys can be marked as suitable for transport encryption, storage encryption, or both, i.e., universal. [default: universal]
.TP
\fB\-\-can\-sign\fR
Add a signing\-capable subkey (default)
.TP
\fB\-\-cannot\-authenticate\fR
Add no authentication\-capable subkey
.TP
\fB\-\-cannot\-encrypt\fR
Add no encryption\-capable subkey
.TP
\fB\-\-cannot\-sign\fR
Add no signing\-capable subkey
.TP
\fB\-\-expiry\fR=\fIEXPIRY\fR
Define EXPIRY for the key as ISO 8601 formatted string or custom duration. If an ISO 8601 formatted string is provided, the validity period reaches from the reference time (may be set using `\-\-time`) to the provided time. Custom durations starting from the reference time may be set using `N[ymwds]`, for N years, months, weeks, days, or seconds. The special keyword `never` sets an unlimited expiry.
.TP
\fB\-o\fR, \fB\-\-output\fR=\fIFILE\fR
Write to FILE or stdout if omitted
.TP
\fB\-\-rev\-cert\fR=\fIFILE or \-\fR
Write the revocation certificate to FILE. mandatory if OUTFILE is `\-`. [default: <OUTFILE>.rev]
.TP
\fB\-u\fR, \fB\-\-userid\fR=\fIEMAIL\fR
Add a userid to the key
.TP
\fB\-\-with\-password\fR
Protect the key with a password
.SS "Global options"
See \fBsq\fR(1) for a description of the global options.
.SH EXAMPLES
.PP

.PP
First, generate a key
.PP
.nf
.RS
sq key generate \-\-userid '<juliet@example.org>' \\
.RE
.RS
.RS
\-\-output juliet.key.pgp
.RE
.RE
.PP
.fi

.PP
Then, extract the certificate for distribution
.PP
.nf
.RS
sq toolbox extract\-cert \-\-output juliet.cert.pgp juliet.key.pgp
.RE
.PP
.fi

.PP
Generate a key protecting it with a password
.PP
.nf
.RS
sq key generate \-\-userid '<juliet@example.org>' \-\-with\-password
.RE
.PP
.fi

.PP
Generate a key with multiple userids
.PP
.nf
.RS
sq key generate \-\-userid '<juliet@example.org>' \\
.RE
.RS
.RS
\-\-userid 'Juliet Capulet'
.RE
.RE
.PP
.fi

.PP
Generate a key whose creation time is June 9, 2011 at midnight UTC
.PP
.nf
.RS
sq key generate \-\-time 20110609 \-\-userid Noam \\
.RE
.RS
.RS
\-\-output noam.pgp
.RE
.RE
.fi
.SH "SEE ALSO"
.nh
\fBsq\fR(1), \fBsq\-key\fR(1).
.hy
.PP
For the full documentation see <https://book.sequoia\-pgp.org>.
.SH VERSION
0.34.0 (sequoia\-openpgp 1.19.0)
